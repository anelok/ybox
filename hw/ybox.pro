update=Fri Sep 27 07:25:32 2013
last_client=pcbnew
[eeschema]
version=1
LibDir=
NetFmtName=
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=../../kicad-libs/components/r
LibName2=../../kicad-libs/components/led
LibName3=../../kicad-libs/components/powered
LibName4=../../kicad-libs/components/micro_usb_b
LibName5=../../kicad-libs/components/usb-a-s4
LibName6=../../kicad-libs/components/device_sot
LibName7=../../kicad-libs/components/pwr
LibName8=../../kicad-libs/components/c
LibName9=../../kicad-libs/components/kl25-32
LibName10=../../kicad-libs/components/cc2543
LibName11=../../kicad-libs/components/balun-smt6
LibName12=../../kicad-libs/components/xtal-4
LibName13=../../kicad-libs/components/testpoint
LibName14=../../kicad-libs/components/varistor
LibName15=../../kicad-libs/components/antenna
LibName16=../../kicad-libs/components/switch
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill="    0.600000"
PadDrillOvalY="    0.600000"
PadSizeH="    1.500000"
PadSizeV="    1.500000"
PcbTextSizeV="    1.500000"
PcbTextSizeH="    1.500000"
PcbTextThickness="    0.300000"
ModuleTextSizeV="    1.000000"
ModuleTextSizeH="    1.000000"
ModuleTextSizeThickness="    0.150000"
SolderMaskClearance="    0.000000"
SolderMaskMinWidth="    0.000000"
DrawSegmentWidth="    0.200000"
BoardOutlineThickness="    0.100000"
ModuleOutlineThickness="    0.150000"
[pcbnew/libraries]
LibDir=
LibName1=../../kicad-libs/modules/stdpass
LibName2=../../kicad-libs/modules/zx62-b-5pa
LibName3=../../kicad-libs/modules/sot-323
LibName4=../../kicad-libs/modules/usb_a_rcpt_mid
LibName5=../../kicad-libs/modules/qfn
LibName6=../../kicad-libs/modules/pads
LibName7=../../kicad-libs/modules/meander-2450MHz
LibName8=../../kicad-libs/modules/xtal-4
LibName9=../../ben-wpan/modules/0805-6
LibName10=../../kicad-libs/modules/fiducial
LibName11=../../kicad-libs/modules/sw-spdt-smt
