/*
 * ybox.c - Y-Box control tool (for development)
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <usb.h>

#include "usbopen.h"

#include "../fw/ep0.h"		/* for YBOX_{GET,PUT,PEEK,POKE} */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae71


static usb_dev_handle *dev;


static int poke(const char *arg)
{
	const char *p;
	char *end;
	unsigned addr, value;
	int res;

	p = strchr(arg, '=');
	if (!p)
		return 0;
	addr = strtoul(arg, &end, 0);
	if (end != p)
		return -1;
	value = strtoul(p+1, &end, 0);
	if (*end)
		return -1;

	res = usb_control_msg(dev, YBOX_POKE & 0xff, YBOX_POKE >> 8,
	    value, addr, NULL, 0, 1000);
	if (res < 0) {
		fprintf(stderr, "YBOX_POKE: %d\n", res);
		exit(1);
	}
	return 1;
}


static int peek(const char *arg)
{
	const char *p;
	char *end;
	unsigned addr;
	int res;

	p = strchr(arg, 'q');
	if (!p || p[1])
		return 0;
	addr = strtoul(arg, &end, 0);
	if (end != p)
		return -1;

	res = usb_control_msg(dev, YBOX_PEEK & 0xff, YBOX_PEEK >> 8,
	    0, addr, NULL, 0, 1000);
	if (res < 0) {
		fprintf(stderr, "YBOX_PEEK: %d\n", res);
		exit(1);
	}
	return 1;
}


static void send_message(const uint8_t *buf, uint8_t n_args, bool cmd)
{
	int res;

	res = usb_control_msg(dev, YBOX_PUT, 0,
	    cmd, 0, (char *) buf, n_args, 1000);
	if (res < 0) {
		fprintf(stderr, "YBOX_PUT: %d\n", res);
		exit(1);
	}
}


static int make_message(char **args, int n_args, bool cmd)
{
	uint8_t buf[n_args];
	char *end;
	int i;
	unsigned tmp;

	for (i = 0; i != n_args; i++) {
		tmp = strtoul(args[i], &end, 0);
		if (*end || tmp > 255)
			return -1;
		buf[i] = tmp;
	}
	send_message(buf, n_args, cmd);
	return 0;
}


static int send_cw(char **args, int n_args)
{
	unsigned f;
	char *end;
	uint8_t buf[3];

	switch (n_args) {
	case 0:
		f = 0;
		break;
	case 1:
		f =  strtoul(args[0], &end, 0);
		if (*end || (f && f < 2379) || f > 2495)
			return -1;
		break;
	default:
		return -1;
	}

	buf[0] = 4;
	buf[1] = f;
	buf[2] = f >> 8;
	send_message(buf, 3, 1);
	return 0;
}


static void touch(void)
{
	uint16_t buf;
	int res;

	res = usb_control_msg(dev, YBOX_TOUCH & 0xff, YBOX_TOUCH >> 8,
	    0, 0, (char *) &buf, 2, 1000);
	if (res < 0) {
		fprintf(stderr, "YBOX_TOUCH: %d\n", res);
		exit(1);
	}
	if (res)
		printf("%u\n", buf);
	exit(0);
}


static void usage(const char *name)
{
	fprintf(stderr,
    "usage: %s [addr=value...] [addrq ...] green red\n"
    "       %s [-c|-d] value ...\n"
    "       %s -f [MHz]   (2379-2495 MHz)\n"
    , name, name, name);
	exit(1);
}


int main(int argc, char **argv)
{
	uint8_t buf[128];
	int res, i;

	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}

	res = usb_claim_interface(dev, 0);
	if (res) {
		fprintf(stderr, "usb_claim_interface: %d\n", res);
		return 1;
	}

	if (argc > 1) {
		if (!strcmp(argv[1], "-c")) {
			if (make_message(argv+2, argc-2, 1))
				usage(*argv);
			return 0;
		}
		if (!strcmp(argv[1], "-d")) {
			if (make_message(argv+2, argc-2, 0))
				usage(*argv);
			return 0;
		}
		if (!strcmp(argv[1], "-f")) {
			if (send_cw(argv+2, argc-2))
				usage(*argv);
			return 0;
		}
		if (!strcmp(argv[1], "-t")) {
			touch();
			return 0;
		}
	}

	for (i = 1; i != argc; i++)  {
		res = poke(argv[i]);
		if (res < 0)
			usage(*argv);
		if (res)
			continue;
		res = peek(argv[i]);
		if (res < 0)
			usage(*argv);
		if (res)
			continue;
		break;
	}
	switch (argc-i) {
	case 2:
		res = usb_control_msg(dev, YBOX_PUT, 0,
		    atoi(argv[i]), atoi(argv[i+1]), NULL, 0, 1000);
		if (res < 0) {
			fprintf(stderr, "YBOX_PUT: %d\n", res);
			return 1;
		}
		break;
	case 0:
		if (argc != 1)
			break;
		res = usb_control_msg(dev, YBOX_GET, 0, 0, 0,
		    (char *) buf, sizeof(buf), 1000);
		if (res < 0) {
			fprintf(stderr, "YBOX_GET: %d\n", res);
			return 1;
		}
		for (i = 0; i != res; i++) {
			if (buf[i] < ' ' || buf[i] >= 127)
				printf("\\%03o", buf[i]);
			else
				putchar(buf[i]);
		}
		putchar('\n');
		break;
	default:
		usage(*argv);
	}

	return 0;
}
