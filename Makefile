.PHONY:		all dsv AUTHORS

all:		AUTHORS

dsv:
		dsv setup BOOKSHELF

AUTHORS:
		{ echo "*** MACHINE-GENERATED. DO NOT EDIT ! ***"; echo; \
		  ../wernermisc/bin/authors; } >AUTHORS || { rm -f $@: exit 1; }
