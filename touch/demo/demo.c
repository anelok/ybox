/*
 * demo.c - Y-Box touch sensor demo (for development)
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include "usbopen.h"

#include "../../fw/ep0.h"	/* for YBOX_TOUCH */


#define	USB_VENDOR	0x20b7
#define	USB_PRODUCT	0xae71

#define	XRES	128
#define	YRES	64

static usb_dev_handle *dev;



/* ----- Touch sensor sampling --------------------------------------------- */


static uint16_t sample(void)
{
	static uint16_t buf;
	int res;

	do {
		res = usb_control_msg(dev, YBOX_TOUCH & 0xff, YBOX_TOUCH >> 8,
		    0, 0, (char *) &buf, 2, 1000);
		if (res < 0) {
			fprintf(stderr, "YBOX_TOUCH: %d\n", res);
			exit(1);
		}
	} while (!res);

	printf("%u\n", buf);

	return buf;
}


/* ----- Sample -> Position and up/down ------------------------------------ */


#define	POSITIONS	128
#define	SHIFT		16

#define	LOW_SAMPLES	16
#define	LOW_EWMA	16


static uint32_t low;
static uint16_t low_buf = 0xffff;
static uint8_t low_samples = LOW_SAMPLES;


static void touch_init(void)
{
	uint16_t s = sample();

	low = LOW_EWMA*s;
}


#if 0
#define	UP_MAX	10400
#define	LEFT	11055
#define	RIGHT	10555
#endif

//#define	UP_MAX	10450
#define	UP_MAX	10500
//#define	LEFT	10850
#define	LEFT	10860
//#define	RIGHT	10555
#define	RIGHT	10620
#define	HARD	11050


static bool touch(uint8_t *pos)
{
	uint16_t s;

	s = sample();

	if (s <= UP_MAX)
		return 0;
	if (s > HARD)
		exit(0);
	if (s < RIGHT)
		*pos = POSITIONS-1;
	else if (s > LEFT)
		*pos = 0;
	else
		*pos = (POSITIONS-1)-
		    ((s-RIGHT)*((POSITIONS << SHIFT)/(LEFT-RIGHT)) >> SHIFT);
	return 1;
}


#if 0
static bool touch(uint8_t *pos)
{
	uint32_t low, low_sum;
	uint8_t low_samples;

	s = sample();

	if (s < low_buf)
		low_buf = s;
	if (!--low_samples) {
		low = ((LOW_EWMA-1)*low + low_buf) / LOW_EWMA;
		low_buf = 0xffff;
		low_samples = LOW_SAMPLES;
	}
}
#endif


/* ----- GUI --------------------------------------------------------------- */


static void gui(void)
{
	SDL_Surface *surf;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "SDL_init: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	surf = SDL_SetVideoMode(XRES, YRES, 0, SDL_SWSURFACE);

	while (1) {
		uint8_t pos;
		bool down;

		down = touch(&pos);

		SDL_LockSurface(surf);
		SDL_FillRect(surf, NULL, SDL_MapRGB(surf->format, 0, 0, 0));
		
		if (down)
			vlineColor(surf, pos, 0, YRES-1, 0xffffffff);

		SDL_UnlockSurface(surf);
		SDL_UpdateRect(surf, 0, 0, 0, 0);
	}
}


/* ----- Main -------------------------------------------------------------- */


int main(int argc, char **argv)
{
	usb_unrestrict();
	dev = open_usb(USB_VENDOR, USB_PRODUCT);
	if (!dev) {
		fprintf(stderr, ":-(\n");
		return 1;
	}
	gui();
	return 0;
}

