/*
 * fw/cc/comm.c - CC254x to KL2x communication
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>
#include <stdint.h>

#include "sfr.h"
#include "comm.h"


/* ----- SPI interface ----------------------------------------------------- */


static bool nreq_read(void)
{
	return P2 & (1 << 2);
}


static void nint_set(bool on)
{
	if (on)
		P2 |= 1 << 1;
	else
		P2 &= ~(1 << 1);
}


static void spi_init(void)
{
	P0SEL |= 0x3c;		/* SCLK, nSEL, MOSI, MISO */
	U0GCR = U0GCR_ORDER;	/* SPI is MSb first */
	U0UCR = U0UCR_FLUSH;	/* flush USART */
	U0CSR =
	    U0CSR_SLAVE		/* SPI slave */
	    | U0CSR_RE;		/* enable receiver */

	P2DIR |= 1 << 1;	/* DD/nINT on P2_1 */
}


/* ----- Transfer protocol ------------------------------------------------- */


static enum state {
	S_IDLE,
} state = S_IDLE;

static const void *tx_buf;
static uint8_t tx_len;


uint8_t comm_poll(void *buf, uint8_t size)
{
	const uint8_t *tx = tx_buf;
	uint8_t *b = buf;
	uint8_t cmd, len;
	uint8_t i;

	if (!tx_len && nreq_read())
		return 0;

	(void) U0DBUF; /* make sure the buffer is empty */
	U0DBUF = tx_len;

	nint_set(0);
	while (!(U0CSR & U0CSR_RX_BYTE));
	nint_set(1);

	cmd = U0DBUF;
	len = cmd & 0x7f;
	if (tx_len > len)
		len = tx_len;

	for (i = 0; i != len; i++) {
		if (i < tx_len)
			U0DBUF = *tx++;
		else
			U0DBUF = 0;
		while (!(U0CSR & U0CSR_RX_BYTE));
		if (i < size)
			*b++ = U0DBUF;
		else
			(void) U0DBUF;
	}

	while (U0CSR & U0CSR_ACTIVE);
	tx_len = 0;

	return cmd;
}


bool comm_send(const void *buf, uint8_t len)
{
	if (tx_len)
		return 0;
	tx_buf = buf;
	tx_len = len;
	return 1;
}


void comm_init(void)
{
	spi_init();
}
