/*
 * fw/cc/radio.h - CC25xx radio control
 *
 * Written 2014 by Werner Almesberger
 * Copyright 2014 Werner Almesberger
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


#include <stdbool.h>

#include "cc25xx-radio.h"
#include "cc25xx-radio-regs.h"
#include "misc.h"
#include "radio.h"


void setup_radio(unsigned f)
{
	/* cancel any previous command */
	RFST = CMD_SHUTDOWN;

delay();

	/* reset LLE */
	LLECTRL = 0;

	if (!f)
		return;

	LLECTRL = LLECTRL_LLE_EN;
	/* @@@ data sheet specifies no delay here. Wonder if that's correct. */
delay();

	/* frequency is set by hw regs, keep synthesizer on at task end */

	PRF_CHAN = 127 | PRF_CHAN_FREQ_SYNTH_ON;

	FREQCTRL = f-2379;

	MDMCTRL0 =
	    0*MDMCTRL0_PHASE_INVERT
	    | MDMCTRL0_MODULATION_GFSK_250kHz_1Mbps
	    | MDMCTRL0_TX_IF;	/* for continuous wave (CW) */

	MDMTEST1 =
	    MDMTEST1_TX_TONE_0Hz	/* TX IF + 0 MHz */
	    | 0*MDMTEST1_RX_IF;		/* RX IF + 1 MHz */

	/* initialize RAM-based registers */

	PRF_CHAN = 127;
	PRF_TASK_CONF = 0;
	PRF_FIFO_CONF = 0;
	PRF_PKT_CONF = 0;
	PRF_CRC_LEN = 0;
	//PRF_RSSI_LIMIT = 0;
	//PRF_RSSI_COUNT = 0;
	//PRF_CRC_INIT
	// PRF_W_INIT
	PRF_RETRANS_CNT = 1;
	PRF_TX_DELAY = 0;
	PRF_RETRANS_DELAY = 0;
	PRF_SEARCH_TIME = 0; /* forever */
	PRF_RX_TX_TIME = 0;
	PRF_TX_RX_TIME = 0;
	PRF_RADIO_CONF = 0;

	/* enable whitening */

	BSP_MODE = BSP_MODE_W_PN7_EN | BSP_MODE_W_PN9_EN;

	/* section 23.12.2 updates */

	FRMCTRL0 = 0x43;
	TXPOWER = 0xe5;
	MDMCTRL1 = 0x48;
	MDMCTRL2 = 0xc0;
	MDMCTRL3 = 0x63;
	RXCTRL = 0x33;
	FSCTRL = 0x55;
	LNAGAIN = 0x3a;
	ACOMPQS = 0x16;
	TXFILTCFG = 0x07;

	RFST = CMD_TX_TEST;
}
