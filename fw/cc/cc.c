#include <stdbool.h>
#include <stdint.h>

#include "sfr.h"
#include "comm.h"
#include "op.h"
#include "radio.h"


/* ----- Clock output ------------------------------------------------------ */


static void clock_output(void)
{
	CLKCONCMD = CLKCONCMD_OSC32K;
	P1SEL |= 1 << 3;
	T3CTL = T3CTL_CLR;
	T3CCTL1 =
	    T3CCTL1_MODE
	    | T3CCTL1_CMP_TOG_CMP;
	T3CC0 = 0;
	T3CTL =
	    T3CTL_MODE_MOD
	    | T3CTL_CLR
	    | T3CTL_START
	    | T3CTL_DIV_8;
}


/* ----- LED --------------------------------------------------------------- */


static void led(bool on)
{
	if (on)
		P1 |= 1 << 0;
	else
		P1 &= ~(1 << 0);
}


static void init_led(void)
{
	P1 &= ~(1 << 0);
	P1DIR |= 1 << 0;	/* LED on P1_0 */
}


/* ----- GPIOs ------------------------------------------------------------- */


static void command(const uint8_t *buf, uint8_t len)
{
//	static char tx[] = "hello, world !";
	static uint8_t res;
	volatile __xdata uint8_t *xptr;

	if (!len)
		return;
	switch (buf[0]) {
	case OP_POKE:
		if (len < 4)
			return;
		xptr = (volatile __xdata uint8_t *) (buf[1] | buf[2] << 8);
		*xptr = buf[3];
		return;
	case OP_PEEK:
		if (len < 3)
			return;
		xptr = (volatile __xdata uint8_t *) (buf[1] | buf[2] << 8);
		res = *xptr;
		comm_send(&res, 1);
		return;
	case OP_LED:
		if (len > 1)
			led(buf[1]);
		return;
	case OP_RADIO:
		if (len < 3)
			return;
		setup_radio(buf[1] | buf[2] << 8);
		return;
	default:
		break;
	}
//	comm_send(tx, sizeof(tx)-1);
}


static void data(const uint8_t *buf, uint8_t len)
{
	(void) buf;
	(void) len;
}


void main(void)
{
	static __xdata uint8_t rx_buf[128];
	uint8_t cmd;

	init_led();
	comm_init();
	clock_output();

// @@@ we seem to get stray zero-length command and data after reset
	while (1) {
		cmd = comm_poll(rx_buf, sizeof(rx_buf));
		if (cmd & 0x80)
			command(rx_buf, cmd & 0x7f);
		else
			data(rx_buf, cmd);
	}
}
