/* ----- DUT PCB offset ---------------------------------------------------- */

#define	PCB_X	45.1mm
#define	PCB_Y	21.6mm

/* ----- DUT Components ---------------------------------------------------- */

#include "pos.inc"


#define	USB_B_X	CON1_X
#define	SW_X	SW1_X
